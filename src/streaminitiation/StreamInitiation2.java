/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streaminitiation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
/**
 *
 * @author Zenji
 */
public class StreamInitiation2 {
    
    private List<Integer> returnExams(){
        List<Integer> exams = new ArrayList<>();
        exams.add(50);
        exams.add(20);
        exams.add(35);
        exams.add(40);
        return exams;
    }
    
    public void execute(){
        // foreachとラムダによる実装
        this.returnExams()
            .stream()
            .map(exam -> exam + 5)
            .filter(exam -> exam > 30)
            .sorted()
            .map(exam -> exam + "点")
            .forEach(name -> {
                System.out.println( name + "さん");
            });
    }
}
