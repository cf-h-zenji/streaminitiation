/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streaminitiation.beans;

/**
 *
 * @author Zenji
 */
public class AgencyBean {

    public AgencyBean(int Id, String Code, int Position) {
        this.Id = Id;
        this.Code = Code;
        this.Position = Position;
    }

    private int Id;
    private String Code;
    private int Position;

    public int getId() {
        return Id;
    }

    public void setId(int Id) {
        this.Id = Id;
    }
    
    public String getCode() {
        return Code;
    }

    public void setCode(String Code) {
        this.Code = Code;
    }
    
    public int getPosition() {
        return Position;
    }

    public void setPosition(int Position) {
        this.Position = Position;
    }
}
