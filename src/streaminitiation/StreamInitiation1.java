/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streaminitiation;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;
/**
 *
 * @author Zenji
 */
public class StreamInitiation1 {
    
    private List<String> returnNames(){
        List<String> strings = new ArrayList<>();
        strings.add("Alice");
        strings.add("Bob");
        strings.add("Charie");
        return strings;
    }
    
    public void execute(){
        // foreachとラムダによる実装
        this.returnNames()
            .stream()
            .forEach(name -> {
                System.out.println( name + "さん");
            });
        System.out.println("----------");
        // filter:抽出
        this.returnNames()
            .stream()
            .filter(name -> name.length() > 3)
            .forEach(name -> {
                System.out.println( name + "さん");
            });
    }
}
