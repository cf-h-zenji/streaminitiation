/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package streaminitiation;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Stream;
import static jdk.nashorn.internal.objects.NativeArray.map;
import streaminitiation.beans.AgencyBean;

/**
 *
 * @author Zenji
 */
public class StreamInitiation3 {
    public Map<AgencyBean,String> returnHashMap(){
        Map<AgencyBean,String> hashMap = new HashMap<>();
        hashMap.put(new AgencyBean(1,"001-1",1), "Alice");
        hashMap.put(new AgencyBean(2,"002-1",1), "Bob");
        hashMap.put(new AgencyBean(2,"002-2",2), "Charie");
        return hashMap;
    }
    
    public void execute(){
    }
}
